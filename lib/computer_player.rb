require "human_player"

class ComputerPlayer


  attr_reader :name
  attr_accessor :mark, :board

  def initialize(name)
    @name = name
  end

  def display(board)
    @board = board
  end

  POSSIBLE_WINS = [
    [[0,0],[0,1],[0,2]],
    [[1,0],[1,1],[1,2]],
    [[2,0],[2,1],[2,2]],
    [[0,0],[1,0],[2,0]],
    [[0,1],[1,1],[2,1]],
    [[0,2],[1,2],[2,2]],
    [[0,0],[1,1],[2,2]],
    [[2,0],[1,1],[0,2]]
  ]

  def winning_move(mark)

    POSSIBLE_WINS.each_with_index do |possible_win, i|
      o_counter = 0

      possible_win.each_with_index do |pos, j|
        o_counter += 1 if @board[pos] == mark
      end

      if o_counter == 2
        possible_win.each_with_index do |pos, j|
          return pos if @board[pos] == nil
        end
      end

    end
    false

  end

  def get_move
    return winning_move(@mark) if winning_move(@mark)
    return [rand(0...@board.grid.length), rand(0...@board.grid.length)]
  end


end
