class Board

  attr_accessor :grid

  def initialize(grid=Array.new(3) { Array.new(3) })
    @grid = grid
  end

  def [](pos)
    row, col = pos
    @grid[row][col]
  end

  def []=(pos, mark)
    row, col = pos
    @grid[row][col] = mark
  end

  def place_mark(pos, mark)
    self[pos] = mark
  end

  def empty?(pos)
    self[pos] == nil
  end

  def row_winner
    @grid.each do |row|
      return :X if row.all? { |mark| mark == :X }
      return :O if row.all? { |mark| mark == :O }
    end
    false
  end

  def left_diagonal_winner
    left_diag_marks = []
    i = 0
    while i < @grid.length
      left_diag_marks << @grid[i][i]
      i += 1
    end

    return :O if left_diag_marks.all? { |mark| mark == :O }
    return :X if left_diag_marks.all? { |mark| mark == :X }
    false
  end

  def right_diagonal_winner
    right_diag_marks = []
    row_pos = 0
    col_pos = @grid.length - 1
    while row_pos < @grid.length && col_pos >= 0
      right_diag_marks << @grid[row_pos][col_pos]
      row_pos += 1
      col_pos -=1
    end

    return :O if right_diag_marks.all? { |mark| mark == :O }
    return :X if right_diag_marks.all? { |mark| mark == :X }
    false
  end

  def column_winner
    @grid.each_with_index do |row, col_pos|
      column = []
      row.each_with_index do |col, row_pos|
        column << @grid[row_pos][col_pos]
      end
      return :O if column.all? { |mark| mark == :O }
      return :X if column.all? { |mark| mark == :X }
    end
    false
  end

  def cats_game?
    all_marks = []
    @grid.each do |row|
      row.each do |mark|
        all_marks << mark
      end
    end
    nil_marks = all_marks.reject {|mark| mark != nil}
    return true if nil_marks.length < 3 && !winner
    false
  end

  def winner
    return row_winner if row_winner
    return column_winner if column_winner
    return left_diagonal_winner if left_diagonal_winner
    return right_diagonal_winner if right_diagonal_winner
  end

  def over?
    return true if cats_game?
    return true if winner
    false
  end


end
