class HumanPlayer

  attr_accessor :name

  def initialize(name)
    @name = name
  end

  def get_move
    puts "Where would you like to place your mark?"
    answer = gets.chomp.delete!(",").split
    pos = [answer[0].to_i, answer[1].to_i]
  end

  def display(board)
    p board
  end

end
